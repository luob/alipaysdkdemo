﻿//
//  PartnerConfig.h
//  AlipaySdkDemo
//
//  Created by ChaoGanYing on 13-5-3.
//  Copyright (c) 2013年 RenFei. All rights reserved.
//
//  提示：如何获取安全校验码和合作身份者id
//  1.用您的签约支付宝账号登录支付宝网站(www.alipay.com)
//  2.点击“商家服务”(https://b.alipay.com/order/myorder.htm)
//  3.点击“查询合作者身份(pid)”、“查询安全校验码(key)”
//

#ifndef MQPDemo_PartnerConfig_h
#define MQPDemo_PartnerConfig_h

//合作身份者id，以2088开头的16位纯数字
#define PartnerID @"2088801214624518"
//收款支付宝账号
#define SellerID  @"1302721745@qq.com"

//安全校验码（MD5）密钥，以数字和字母组成的32位字符
#define MD5_KEY @"fzw8yxerk2kekdukw6b68pzgywr61lh4"

//商户私钥，自助生成
#define PartnerPrivKey @"MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAMvUykuaZ3Nf3feUMpdT6Z5QPVdXJ7h+ycTOGNEjTAfIuGrLeLrB1YyfWoVo4ov4JhT7vC8yxnA3jgpeHBNoz3MdzmfunRqhZ+I3blTLvU74ZQFgRXzEUuVhf+5mD3zZk6IxmsSvp9SQect0zudRov0dFhaKdJmZrXL65bWCz7h/AgMBAAECgYAKVcjIGpUiQXskwYvrJmMTFuh1VEL8VNlwNC8H8604O9I2AAkoFwn37mCNoSrThAqSR81b60wqhnaaSflMdqUGDxECdo7KAe8qPXMiQdqQarYW4p+pG8uOsxGVJSokUya+00Um7rfS7ugr2QX4ynxld705AV/tvQwI2GIxpFCyaQJBAOs4lRffTacEb3ywCfypdZdds6tca+qIA6v8LiXtHwJUI/UIKA1WEhsji2YSkmcrXJv3jmJ53GXAKaURsIvnDQ0CQQDd1ldhAbST/Zbj7ifpSNS8R9DtB5gEzk9w7amyl4FBjmH2UKaou3vVoJyQ8fcMHmtqCA5jiRkcrbzcfTHWwfC7AkAP6+CLoR3MdExnkDLc20uOV1VWAAd99H7rIUKuZoJ20eMtLsQnQvAuzhUdb5xhR1oZZIRKvhzg9kTbW3srvRPxAkEAwZR6GX+oeX/P/URkdZEFeutcaIq44HC8J/psnBjTRUafO5czZ4h7TnbiSSWshFnDN/qhu7/2Nu1xIiLpq9L/YwJAMGwAdMfER7hpJnkjbHO/zLwGvoMO74F40mOpPE2ImzPLdQHP9QwDV2r30WSbDhRsJsy6dRqJN02xJnxnMa5Kdg=="


//支付宝公钥
#define AlipayPubKey   @"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB"

#endif
